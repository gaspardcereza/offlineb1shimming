function B1perVpeak_ch = ScaleAndRotateB1maps(Bz,VoltageScaleFactor)
%% *************************************************************************
% FUNCTION:    B1perVpeak_ch = ScaleAndRotateB1maps(Bz,VoltageScaleFactor)
% 
% DESCRIPTION: This function will scale B1 fields by the voltage scaling
% factor (as determined by CalcB1ScalingFactor.m). It also rotates the B1
% maps into a convention often used by the B1 shimming and SAR scripts
% maintained by KMG. It converts B1 maps to nT/Vpeak.
%
% INPUTS:      Bz - B1 map as save from PrepareFields.m.
%               (nPts_x x nPts_y x nPts_z x nCoils) [T/W_peak]
%              VoltageScaleFactor - Voltage scale factor for each coil 
%               element as created from CalcB1ScalingFactor.m (1 x nCoils) 
%               [V]. If not specified, this will default to ones (1xnCoils)
% 
% OUTPUTS:     B1perVpeak_ch - B1 matrix in nT/Vpeak, scaled by the voltage
%              scaling factor, and re-oriented.
% 
% Created by:  Kyle M. Gilbert
%              April 2020 (KMG)
% 
% Copyright:   Kyle M. Gilbert
%% ************************************************************************

% Rotate the simulated maps to radiological space
B1perVpeak_ch = rot90(Bz,3);
B1perVpeak_ch = fliplr(B1perVpeak_ch);

% Scale simulated data from T/W_peak to T/Vpeak
B1perVpeak_ch = B1perVpeak_ch ./ sqrt(50); % Convert from T/W_peak to T/Vpeak
B1perVpeak_ch = B1perVpeak_ch .* 1e9;  % Convert from T/Vpeak to nT/Vpeak

% Scale maps
for coil_i = 1:size(B1perVpeak_ch,4)
    % Apply magnitude scaling to the simulated data
    B1perVpeak_ch(:,:,:,coil_i) = B1perVpeak_ch(:,:,:,coil_i) .* VoltageScaleFactor(coil_i);
end