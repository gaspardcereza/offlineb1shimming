Functions and example data for offline B1+ shimming
______________
The main function is B1Shim.m. An example of the input parameters are found in OfflineShimmingParams_*.m. Sample .mat files are inlcuded for SAR and B1 fields. These .mat files use lfs tracking.