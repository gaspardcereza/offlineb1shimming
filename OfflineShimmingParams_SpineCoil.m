% Offline shimming params for spine coil

B1_fullpath = '/Users/kmgilbert/Documents/ResearchScientist/VOPs/SpineCoil/Phantom_8chDipole/Simulated_B1/B1.mat';
SAR_fullpath = '/Users/kmgilbert/Documents/ResearchScientist/VOPs/SpineCoil/Phantom_8chDipole/Simulated_SAR/SARMatrices.mat'; % Location to store new .mat file
SED = 1.5;
% CPphases = [-0.0000 -8.3687 -90.1268 -137.7138 -182.6746 -209.7271
% -225.1618 -207.6175]; % 7ch diple with loop
CPphases = [0.0000 -13.0914 -89.3221 -143.9754 -193.2921 -204.7722 -201.2067 57.5821]; % 8ch dipole
algorithm = 1;
PhaseOnly = 0;
lambda = 1e12; %5e7 will give you about equal weighting between uniformity and power
VoltageScaleFactor = [];
Constrained = 0;

B1Shim(B1_fullpath, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained, VoltageScaleFactor);