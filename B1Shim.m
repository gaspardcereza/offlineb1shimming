function B1Shim(B1_fullpath, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained, VoltageScaleFactor)
%% *************************************************************************
% FUNCTION:    B1Shim(B1_fullpath, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained, VoltageScaleFactor)
% 
% DESCRIPTION: This function will determine TxScaleFactors to optimize the 
%              B1+ shim. Simulated B1+ and SAR maps are read in. A 
%              non-linear optimization is used to minimize the cost 
%              function while constraining the maximum 10-g-averaged 
%              SAR to SED-fold higher or lower than the SAR of the CP
%              (TrueForm) mode. The available cost functions, algorithms, 
%              are listed below. The B1 shim optimization can also be run
%              unconstrained by setting the 'Constrained' parameter to 0.
%
% INPUTS:      B1_fullpath - .mat file for the 3D B1+ maps of each channel
%               (nPts_x x nPts_y x nPts_z x nCoils) [nT/V]
%              SAR_fullpath - .mat file for the local SAR matrices
%                (nCoils x nCoils x nVoxels)
%              algorithm - 1: minimizes the coefficient of variation
%                          2: minimizes l2-norm of B1 field and regularizes
%                             on the mean B1 squared (i.e., power) with the
%                             Tikhonov-regularization parameter, lambda
%                          3: maximizes the minimum B1
%              SED - the factor to which the local SAR of the optimized
%               shim solution can be higher than the SAR of the CP mode. If
%               this is set too low, the optimization may not be able to
%               find a solution that satisifes this constraint.
%              CPphases - vector of phases for the CP mode [deg] (1 x
%               nCoils). If empty, this will default to zero phase.

%              PhaseOnly - flag if you want to optimize only on the phases.
%               If empty, this will default to 0.
%              lambda - Tikhonov-regularization parameter for algorithm #2.
%               If empty, this will default to 0.
%              Constrained - flag as to whether you want the optimization
%               constrained by SAR or not. The unconstrained optimization
%               is faster. If empty, this will default to zero (unconstrained)
%              VoltageScaleFactor - Voltage scale factor for each coil 
%               element as created from CalcB1ScalingFactor.m (1 x nCoils) 
%               [V]. If empty, this will default to ones (1xnCoils)
% 
% OUTPUTS:     Prints to screen the CP mode and optimized performance specs
%              of the solutions. It also plots the CP mode B1+ map and the 
%              optimized B1+ map.
% 
% Created by:  Kyle M. Gilbert
%              April 2020 (KMG)
% 
% Copyright:   Kyle M. Gilbert
%% ************************************************************************

narginchk(4, 9);

% Read in the simulated B1+ maps
% Check to see if the B1 maps have already been rotated into the proper
% orientation and if they have been scaled appropriately. The convention is
% that the field maps are named 'Bz' directly out of CST and renamed to
% 'B1perVpeak_ch' once they have been scaled and re-orientated.
variableInfo = who('-file', B1_fullpath);
if ismember('B1perVpeak_ch', variableInfo)
   load(B1_fullpath, 'B1perVpeak_ch');
elseif ismember('Bz', variableInfo)
   load(B1_fullpath, 'Bz');
  
    if nargin < 9 || isempty(VoltageScaleFactor)
        VoltageScaleFactor = ones(1,size(Bz,4));
    end
    B1perVpeak_ch = ScaleAndRotateB1maps(Bz,VoltageScaleFactor);
end

% Number of coils
nCoils = size(B1perVpeak_ch,4);

% If inputs don't exist, set to default values
if nargin < 8
   Constrained = false;
   if nargin < 7
      lambda = 0;
      if nargin < 6
         PhaseOnly = false;
         if nargin < 5
            CPphases = zeros(1,nCoils);
         end
      end
   end
end
if isempty(PhaseOnly)
    PhaseOnly = false;
end
if isempty(lambda)
    lambda = 0;
end
if isempty(CPphases)
    CPphases = zeros(1,nCoils);
end
if isempty(Constrained)
    Constrained = false;
end

% Save the full B1 maps for later
B1perVpeak_ch_ORIG = B1perVpeak_ch; % For plotting later

% Plot the CP-mode B1+ map over the entire imaging volume. This can be used
% by the user to determine over which slices they want to shim.
% Normalize the TxScaleFactors (all weights of one), as this is the
% convention on the Siemens scanner.
TxScaleFactors_CP = ones(1,nCoils) .* exp(1i*deg2rad(CPphases)) ./ norm(ones(1,nCoils));

% Compute the combined B1 map in CP mode
CombinedB1Map_CP = CalcCombinedB1Map(TxScaleFactors_CP,B1perVpeak_ch_ORIG);

% Determine the maximum SAR for CP mode
% SAR = U'AU, where U is the excitation vector and A is the local SAR
% matrices (nCoils x nCoils x nVoxels)
load(SAR_fullpath, 'SARMatrices');
LocalSAR_CP = real(sum(bsxfun(@times, conj(TxScaleFactors_CP(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors_CP(:).'),2)), 1)); % SAR for each 10-g-average volume
maxSARLimit = max(LocalSAR_CP(:)) .* SED; % Maximum local SAR for given excitation [W/kg]

% Plot the CP mode
cmax = max(CombinedB1Map_CP(:));
figure;
montage(CombinedB1Map_CP,'Size',[ceil(sqrt(size(CombinedB1Map_CP,3))) ceil(sqrt(size(CombinedB1Map_CP,3)))]);
axis image off; grid off;
colormap parula
caxis([0 cmax])
title('B1+ efficiency: CP mode')
drawnow

% Prompt the user to specificy the slices over which they would like to
% shim.
prompt = 'Over what range of slices would you like to B1+ shim?   ';
ShimmedSlices = input(prompt);

% Reduce the FOV over which to B1+ shim
B1perVpeak_ch = squeeze(B1perVpeak_ch(:,:,ShimmedSlices,:));

% Calculate the combined B1 map over the reduced FOV
CombinedB1Map_CP_reduced = CalcCombinedB1Map(TxScaleFactors_CP,B1perVpeak_ch);

% Plot the CP mode over the prescribed slices
figure;
montage(CombinedB1Map_CP_reduced,'Size',[ceil(sqrt(size(CombinedB1Map_CP_reduced,3))) ceil(sqrt(size(CombinedB1Map_CP_reduced,3)))]);
axis image off; grid off;
colormap parula
caxis([0 cmax])
h = colorbar;
h.LineWidth = 2;
h.FontSize = 18;
ylabel(h,'B1+ [nT/V]');
title('B1+ efficiency: CP mode')
drawnow

% Define the coeffs as the real and imaginary components of the
% TxScaleFactors. The coeffs will be optimized in the minimization
% algorithm.
% Define the coeffs of the CP mode as the starting point for the
% optimization.
coeffs_CP(1:nCoils) = real(TxScaleFactors_CP);
coeffs_CP(nCoils+1:2*nCoils) = imag(TxScaleFactors_CP);

% Lower and upper bound for opimtization on real and imaginary components
% of the TxScaleFactors.
lb = -1.*ones(1,2.*nCoils);
ub = ones(1,2.*nCoils);

% *************************************************************************
% This is the optimization routine.
% Optimize the TxScaleFactors to minimize the coefficient of variation
% while constraining the maximum SAR to SED-fold the maximum SAR of the CP
% mode.
if (Constrained) % Optimization constrained by SAR
    coeffs_Opt = fmincon(@(coeffs) CalcCostFunction(coeffs,B1perVpeak_ch,nCoils,algorithm,PhaseOnly,lambda), coeffs_CP,[],[],[],[],lb,ub,@(coeffs)constrfct(coeffs,SARMatrices,nCoils,maxSARLimit));
else % Unconstrained opitimization (faster)
    coeffs_Opt = fminunc(@(coeffs) CalcCostFunction(coeffs,B1perVpeak_ch,nCoils,algorithm,PhaseOnly,lambda), coeffs_CP);
end
% *************************************************************************

% Determine the optimal TxScaleFactors from the coeffs and calculate the 
% optimized combined B1 map.
TxScaleFactors_Opt = CalcTxScaleFactors(coeffs_Opt);
CombinedB1Map_Opt = CalcCombinedB1Map(TxScaleFactors_Opt,B1perVpeak_ch);

%% CP performance metrics

MeanB1_CP = mean(CombinedB1Map_CP_reduced(logical(CombinedB1Map_CP_reduced)));
Prctile_80_B1_CP = prctile(CombinedB1Map_CP_reduced(logical(CombinedB1Map_CP_reduced)),80);
StdB1_CP = std(CombinedB1Map_CP_reduced(logical(CombinedB1Map_CP_reduced)));
CoeffVar_CP = StdB1_CP ./ MeanB1_CP;

LocalSAR_CP = real(sum(bsxfun(@times, conj(TxScaleFactors_CP(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors_CP(:).'),2)), 1));
maxLocalSAR_CP = max(LocalSAR_CP(:)); % Maximum local SAR for CP excitation [W/kg]
meanB1perSqrtSAR_CP = 0.001 .* (MeanB1_CP ./ sqrt(maxLocalSAR_CP)); % [uT/sqrt(W/kg)]

%% Optimized (shimmed performance metrics)

MeanB1_Opt = mean(CombinedB1Map_Opt(logical(CombinedB1Map_Opt)));
Prctile_80_B1_Opt = prctile(CombinedB1Map_Opt(logical(CombinedB1Map_Opt)),80);
StdB1_Opt = std(CombinedB1Map_Opt(logical(CombinedB1Map_Opt)));
CoeffVar_Opt = StdB1_Opt ./ MeanB1_Opt;

LocalSAR_Opt = real(sum(bsxfun(@times, conj(TxScaleFactors_Opt(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors_Opt(:).'),2)), 1));
maxLocalSAR_Opt = max(LocalSAR_Opt(:)); % Maximum local SAR for given excitation [W/kg]
meanB1perSqrtSAR_Opt = 0.001 .* (MeanB1_Opt ./ sqrt(maxLocalSAR_Opt)); % [uT/sqrt(W/kg)]

%% Print to screen the performance metrics and TxScaleFactors

fprintf(sprintf('\nOriginal CoV [percent]: %0.2f\n',100.*CoeffVar_CP));
fprintf(sprintf('Original mean B1 [nT/V]: %0.2f\n',MeanB1_CP));
fprintf(sprintf('Original 80th percentile B1 [nT/V]: %0.2f\n',Prctile_80_B1_CP));
fprintf(sprintf('Original max local SAR [(W/kg)/V]: %0.5f\n',maxSARLimit));
fprintf(sprintf('Original mean B1/sqrt(max local SAR) [uT/sqrt(W/kg)]: %0.2f\n',meanB1perSqrtSAR_CP));

fprintf(sprintf('\nShimmed CoV [percent]: %0.2f  (%0.1f percent)\n',100.*CoeffVar_Opt, 100 .* (1 - (CoeffVar_Opt./CoeffVar_CP)) ));
fprintf(sprintf('Shimmed mean B1 [nT/V]: %0.2f  (%0.1f percent)\n',MeanB1_Opt, -100.* (1- (MeanB1_Opt ./ MeanB1_CP))  ));
fprintf(sprintf('Shimmed 80th percentile B1 [nT/V]: %0.2f  (%0.1f percent)\n',Prctile_80_B1_Opt, -100.* (1- (Prctile_80_B1_Opt ./ Prctile_80_B1_CP)) ));
fprintf(sprintf('Shimmed max local SAR: %0.5f  (%0.1f percent)\n',maxLocalSAR_Opt, -100.* (1- (maxLocalSAR_Opt ./ max(LocalSAR_CP(:))))  ));
fprintf(sprintf('Shimmed mean B1/sqrt(max local SAR) [uT/sqrt(W/kg)]: %0.2f  (%0.1f percent)\n\n',meanB1perSqrtSAR_Opt, -100.* (1- (meanB1perSqrtSAR_Opt ./ meanB1perSqrtSAR_CP))  ));

% Print to screen the optimized TxScaleFactors
disp('Phase: ')
fprintf('%0.4f ',rad2deg(angle(TxScaleFactors_Opt)));
fprintf(sprintf('\nMag: \n'));
if (PhaseOnly)
    fprintf('%0.4f ',ones(1,nCoils) ./ norm(ones(1,nCoils)));
else
    fprintf('%0.4f ',abs(TxScaleFactors_Opt));
end
fprintf(newline);

%% Plot the optimized map
figure;
montage(CombinedB1Map_Opt,'Size',[ceil(sqrt(size(CombinedB1Map_Opt,3))) ceil(sqrt(size(CombinedB1Map_Opt,3)))]);
axis image off; grid off;
colormap parula
caxis([0 cmax])
h = colorbar;
h.LineWidth = 2;
h.FontSize = 18;
ylabel(h,'B1+ [nT/V]');
title('B1+ efficiency: Shimmed')
drawnow

end

%% ************************************************************************
function cost = CalcCostFunction(coeffs,B1perVpeak_ch,nCoils,algorithm,PhaseOnly,lambda)
% This is the cost function for the non-linear minimization. It calculates
% the cost function of the prescribed slices of the B1 map.

% Calculate the TxScaleFactors from the coeffs
TxScaleFactors = CalcTxScaleFactors(coeffs);

% If you only want a phase-only shim
if (PhaseOnly)
    mag = ones(1,nCoils) ./ norm(ones(1,nCoils)); % constant
    phase = angle(TxScaleFactors); % dynamic
    TxScaleFactors = mag .* exp(1i*phase); % overwrite TxScaleFactors
end

% Compute the B1+ map for the given TxScaleFactors
CombinedB1Map = CalcCombinedB1Map(TxScaleFactors,B1perVpeak_ch);

% Compute common performance metrics
MeanB1 = mean(CombinedB1Map(logical(CombinedB1Map)));
StdB1 = std(CombinedB1Map(logical(CombinedB1Map)));

% COST FUNCTIONS
if algorithm == 1
    % Coeffcient of variation (std/mean)
    cost = StdB1 ./ MeanB1;
    
elseif algorithm == 2
    % Uniformity (l2-norm) with regularization on power (i.e., the inverse 
    % of the efficiency squared)
    absB1 = abs(CombinedB1Map(logical(CombinedB1Map)));
    cost = norm( absB1 - mean(absB1) ).^2 + lambda .* (1 ./ MeanB1.^2);
    
elseif algorithm == 3
    % Maximize the mininmum B1
    cost = 1 ./ min(CombinedB1Map(logical(CombinedB1Map)));
    
end

end

%% ************************************************************************
function CombinedB1Map = CalcCombinedB1Map(TxScaleFactors,B1perVpeak_ch)
% Calculate the combined B1 map with the given TxScaleFactors.

ndim = ndims(B1perVpeak_ch);
if ndim == 4
   p = [2 3 4 1];
else
   p = [2 3 1];
end

CombinedB1Map = abs(sum(bsxfun(@times, B1perVpeak_ch, permute(TxScaleFactors(:), p)), ndim));

end

%% ************************************************************************
function TxScaleFactors = CalcTxScaleFactors(coeffs)
% Create normalized complex scale factors from the 'coeffs' variable, where
% the first half values are the real components of the TxScaleFactors and
% the remaining values are the imaginery components. Normalize by
% the norm of the weights so as not to influence the optimization by having
% a higher or lower weights. This is also the Siemens convention.
TxScaleFactors = complex(coeffs(1:end/2), coeffs(end/2+1:end));
TxScaleFactors = TxScaleFactors ./ norm(TxScaleFactors);

end

%% ************************************************************************
function [c,ceq] = constrfct(coeffs,SARMatrices,maxSAR)
% This is the non-linear constraint on the local SAR. It requires
% the solution to have a maximum SAR less than SED-fold that of the CP mode
% (maxSAR = maxSAR_CPmode * SED).

% Calculate the TxScaleFactors from the coeffs
TxScaleFactors = CalcTxScaleFactors(coeffs);

% Calculate the local SAR for each voxel for the TxScaleFactors
LocalSAR = real(sum(bsxfun(@times, conj(TxScaleFactors(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors(:).'),2)), 1));

% Find the 10-g-averaged voluem that has the maximum local SAR
MaxLocalSAR = max(LocalSAR(:)); % Maximum local SAR for given excitation [W/kg]

% The constraint value. This should always be < 0 to satify the constraint
c = MaxLocalSAR - maxSAR;
ceq = [];

end